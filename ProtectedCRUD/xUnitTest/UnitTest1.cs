using Microsoft.AspNetCore.Mvc;
using ProtectedCRUD.context;
using ProtectedCRUD.Controllers;
using ProtectedCRUD.Models;
using System.Collections.Generic;
using System.Linq;
using System;
using Xunit;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace xUnitTest
{
    public class UnitTest1
    {


        /*public MonContext context;
        private UnitTest1(Montext _context)
        {
            context = _context;
        }*/

            private MonContext GetContextWithData()
            {
                var optionsBuilder = new DbContextOptionsBuilder<MonContext>().UseSqlServer("Server=localhost\\SQLEXPRESS;Database=GoodEats;Integrated Security=True;MultipleActiveResultSets=True").Options;
                //var options = new DbContextOptionsBuilder<MonContext>().Options;
                var context = new MonContext(optionsBuilder);

                /*Plat plat = new Plat();
                plat.imageId = 1;
                plat.libelle = "soupe";
                plat.prix = "4";
                plat.type = "entree";
                plat.description = "une bonne soupe";
                plat.dateCreation = DateTime.Now;
                context.Plat.Add(plat);

                context.SaveChanges();*/

                return context;
            }
            [Fact]
        public async void TestAddPlat()
        {

            using (var context = GetContextWithData())
            using (var controller = new PlatsController(context))
            { 
                Plat plat = new Plat();
                plat.imageId = 1;
                plat.libelle = "soupe";
                plat.prix = "4";
                plat.type = "entree";
                plat.description = "une bonne soupe";
                plat.dateCreation = DateTime.Now;
                context.Plat.Add(plat);

                 context.SaveChanges();
                var result =  context.Plat.OrderByDescending(p => p.id).FirstOrDefault();

                Assert.NotNull(result);
                Assert.Equal(result.libelle, plat.libelle);
            }
           
        }
        /*[Fact]
        public void TestSupprimerPlat()
        {
            using (var context = GetContextWithData())
            using (var controller = new PlatsController(context))
            {
                var result = context.Plat.OrderByDescending(p => p.id).FirstOrDefault();
                var id = result.id;

                Assert.NotNull(result);
                //controller.DeletePlat(id);
                context.Plat.Remove(result);
                context.SaveChangesAsync();
                result = context.Plat.OrderByDescending(p => p.id).FirstOrDefault();
                Assert.NotEqual(id, result.id);

            }
        }*/
    }
}
