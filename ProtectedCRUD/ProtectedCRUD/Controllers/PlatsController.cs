﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProtectedCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtectedCRUD.Controllers
{
    [Route("api/plats")]
    [ApiController]
    public class PlatsController : Controller
    {

        private readonly context.MonContext context;

        public PlatsController(context.MonContext _context)
        {
            context = _context;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Plat>>> GetPlats()
        {
            var plats = await context.Plat.Include(x => x.image).ToListAsync();
            
            if(plats == null || plats.Count < 1)
            {
                return NotFound();
            }

            return plats;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Plat>> GetPlat(int id)
        {
            Plat plat = await context.Plat.FindAsync(id);


            if (plat == null)
            {
                return NotFound();
            }

            return plat;
        }


        [HttpPost("newPlat")]
        public async Task<ActionResult<Plat>> PostPlat(Plat plat)
        {
            Console.WriteLine(plat);
            plat.dateCreation = DateTime.Now;
            plat.imageId = 1;
            context.Plat.Add(plat);
            await context.SaveChangesAsync();

            return plat;
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<string>> DeletePlat(int id)
        {
            Plat plat = await context.Plat.FindAsync(id);

            if (plat == null)
            {
                return NotFound();
            }

            context.Plat.Remove(plat);
            await context.SaveChangesAsync();

            return NoContent();
        }

        public NotFoundResult Category(int v)
        {
            throw new NotImplementedException();
        }

        [HttpPut("update")]
        public async Task<IActionResult> patchPlat(Plat editPlat)
        {
            Plat plat = await context.Plat.FindAsync(editPlat.id);
            if (plat == null)
            {
                throw new ArgumentException("plat not found");
            }

            context.Attach(plat);
           
            plat.libelle = editPlat.libelle;
            plat.type = editPlat.type;
            plat.description = editPlat.description;             
            plat.prix = editPlat.prix;

            //context.Entry(plat).State = EntityState.Modified;

            context.Update(plat);

            await context.SaveChangesAsync();         

            return NoContent();
        }
    }
}
