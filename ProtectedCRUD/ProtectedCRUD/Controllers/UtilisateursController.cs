﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtectedCRUD.context;
using ProtectedCRUD.Models;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using APIRicoh.Services;

namespace ProtectedCRUD.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UtilisateursController : Controller
    {

        private readonly context.MonContext context;

        public UtilisateursController(context.MonContext _context)
        {
            context = _context;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Utilisateur>> GetUser(int id)
        {
            Utilisateur user = await context.Utilisateur.FindAsync(id);


            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        [HttpPost("login")]
        public ActionResult<Utilisateur> Login(Userlog userlog)
        {
            Utilisateur userConnected = context.Utilisateur.Where(userConnected => userConnected.mail.Equals(userlog.mail) && userConnected.mdp.Equals(Hashing.Sha512(userlog.mdp))).FirstOrDefault();
            if (userConnected == null)
            {
                return NotFound();
            }
            return  userConnected;
        }

        [HttpPost]
        public async Task<ActionResult<Utilisateur>> PostUser(Utilisateur utilisateur)
        {
            Console.WriteLine(utilisateur);
            utilisateur.mdp = Hashing.Sha512(utilisateur.mdp);
            utilisateur.id_role = 1;
            
            context.Utilisateur.Add(utilisateur);


            await context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new { id = utilisateur.id }, utilisateur);
        }
    }
}
