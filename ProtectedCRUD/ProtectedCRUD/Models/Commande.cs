﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProtectedCRUD.Models
{
    [Table("Commande")]
    public class Commande
    {
        [Key]
        public int id { get; set; }

        public DateTime date { get; set; }
    }
}
