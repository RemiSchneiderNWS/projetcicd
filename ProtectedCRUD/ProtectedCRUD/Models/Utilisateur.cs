﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectedCRUD.Models
{

    [Table("Utilisateur")]
    public class Utilisateur
    {
        [Key]
       
        public int id { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public string adresse { get; set; }
        public string mail { get; set; }
        public int id_role { get; set; }
        public string telephone { get; set;  }

        public string cp { get; set; }

#pragma warning disable CS8632 // L'annotation pour les types référence Nullable doit être utilisée uniquement dans le code au sein d'un contexte d'annotations '#nullable'.
        public string? jeton { get; set; }
#pragma warning restore CS8632 // L'annotation pour les types référence Nullable doit être utilisée uniquement dans le code au sein d'un contexte d'annotations '#nullable'.

        public string mdp { get; set; }
    }
}
