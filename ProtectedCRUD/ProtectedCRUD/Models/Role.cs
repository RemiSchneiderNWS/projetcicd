﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ProtectedCRUD.Models
{
    [Table("Role")]
    public class Role
    {
        [Key]
        public int id { get; set; }
        public string libelle { get; set; }
       
    }

}
