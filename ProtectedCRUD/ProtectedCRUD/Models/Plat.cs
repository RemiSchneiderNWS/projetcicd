﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProtectedCRUD.Models
{
    [Table("Plat")]
    public class Plat
    {   
        [Key]
        public int id { get; set; }

        public string libelle { get; set; }

        public string type { get; set; }

        public string description { get; set; }

        public DateTime dateCreation { get; set; }

        public string prix { get; set; }


        public int imageId { get; set; }
        public Image image { get; set; }

    }
}
