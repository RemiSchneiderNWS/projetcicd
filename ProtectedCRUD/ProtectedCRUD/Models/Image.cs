﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
namespace ProtectedCRUD.Models
{
    [Table("Image")]
    public class Image
    {
        [Key]
        public int id { get; set; }
        public string libelle { get; set; }
        public string chemin { get; set; }
        public int ordre { get; set; }
    }
}
