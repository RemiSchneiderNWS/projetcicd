﻿using ProtectedCRUD.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProtectedCRUD.context
{
    public class MonContext : DbContext
    {
        public MonContext(DbContextOptions<MonContext> options) : base(options)
        {

        }

        public DbSet<matérial> material { get; set; }
        public DbSet<User> user { get; set; }
        public DbSet<Image> Image { get; set; }
        public DbSet<Plat> Plat { get; set; }
        public DbSet<Commande> Commande { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<Utilisateur> Utilisateur { get; set; }
    }
}
