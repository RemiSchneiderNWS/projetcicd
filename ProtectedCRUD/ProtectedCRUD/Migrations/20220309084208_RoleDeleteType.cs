﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProtectedCRUD.Migrations
{
    public partial class RoleDeleteType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "type",
                table: "Role");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "type",
                table: "Role",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
