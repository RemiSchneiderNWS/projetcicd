﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProtectedCRUD.Migrations
{
    public partial class ImagePlatId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Plat_imageId",
                table: "Plat",
                column: "imageId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Plat_Image_imageId",
                table: "Plat",
                column: "imageId",
                principalTable: "Image",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Plat_Image_imageId",
                table: "Plat");

            migrationBuilder.DropIndex(
                name: "IX_Plat_imageId",
                table: "Plat");

            migrationBuilder.AddColumn<int>(
                name: "PlatId",
                table: "Image",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Image_PlatId",
                table: "Image",
                column: "PlatId");

            migrationBuilder.AddForeignKey(
                name: "FK_Image_Plat_PlatId",
                table: "Image",
                column: "PlatId",
                principalTable: "Plat",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
