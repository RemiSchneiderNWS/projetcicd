﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ProtectedCRUD.Migrations
{
    public partial class ImageOrdre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ordrer",
                table: "Image",
                newName: "ordre");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ordrer",
                table: "Image",
                newName: "ordre");
        }
    }
}
