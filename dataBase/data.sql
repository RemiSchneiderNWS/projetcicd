USE [GoodEats]
GO
Truncate Table Plat 
Truncate Table Image -- Purge d' Image

GO

INSERT INTO [dbo].[Image]
           ([libelle]
           ,[chemin]
           ,[ordre])
     VALUES
           ('saumon','saumon.jpg',0),--1
		   ('rillette','rillette.jpg',0),--2
		   ('escargot','escargot.jpg',0),--3
		   ('riz-dinde','riz-dinde.jpg',0),--4
		   ('riz-boeuf','riz-boeuf.jpg',0),--5
		   ('riz-semoule','riz-semoule.jpg',0),--6
		   ('creme-vanille','creme-vanille.jpg',0),--7
		   ('creme-chocolat','creme-chocolat.jpg',0),--8
		   ('creme-pistache','creme-pistache.jpg',0),--9
		   ('creme-brulee','creme-brulee.jpg',0)--10
GO

USE [GoodEats]
GO

Truncate Table Plat -- Purge de Plat

Go

INSERT INTO [dbo].[Plat]
           ([libelle]
           ,[type]
           ,[description]
           ,[imageId]
           ,[dateCreation]
           ,[prix])
     VALUES
           ('Cr�me brul�e'
           ,'dessert'
           ,'Cr�me � base de flamme.'
           , 10
           ,'2020-01-01'
           ,1),

		   ('Cr�me chocolat'
           ,'dessert'
           ,'Cr�me � base de chocolat.'
           ,8
           ,'2020-04-01'
           ,1),

		   ('Creme vanille'
           ,'dessert'
           ,'Cr�me � base de vanille de turquie.'
           ,7
           ,'2022-01-01'
           ,1),

		   ('Creme pistache'
           ,'dessert'
           ,'Cr�me � base de pistaches.'
           ,9
           ,'2021-01-01'
           ,1),

		   ('Riz dinde'
           ,'plat'
           ,'Du riz et de la dinde !'
           ,4
           ,'2021-06-05'
           ,10),

		   ('Riz boeuf'
           ,'plat'
           ,'Du riz et du boeuf !'
           ,5
           ,'2022-12-09'
           ,12),

		   ('Riz semoule'
           ,'plat'
           ,'De la semoule et du riz !'
           ,6
           ,'2021-05-05'
           ,20),

		   ('Rillette'
           ,'entree'
           ,'Rilletes � base de porc.'
           ,2
           ,'2020-02-01'
           ,2),

		   ('Saumon fum�e'
           ,'entree'
           ,'Du poisson s''appelant saumon.'
           ,1
           ,'2021-01-06'
           ,8),

		   ('Escargot'
           ,'entree'
           ,'C''est pas forc�ment bon.'
           ,3
           ,'2022-03-07'
           ,5)


Go

select * from Plat P inner join Image I on P.imageId = I.id