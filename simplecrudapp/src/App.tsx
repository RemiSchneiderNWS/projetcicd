import React, { useEffect, useMemo, useState } from "react";

import "./App.css";
import Navbar from "./components/Navbar/navbar";
import { BrowserRouter as Router, Redirect, Route } from "react-router-dom";

import Connection from "./components/Connection/connection";
import { ThemeProvider } from "@material-ui/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { createMuiTheme } from "@material-ui/core";
import SignUp from "./components/SignUp/SignUp";
import AccueilConnected from "./components/Home/AccueilConnected";
import AccueilDisconnected from "./components/Home/AccueilDisconnected";
import SignUpSuccess from "./components/SignUp/SignUpSuccess";
import { UserContext } from "./services/context/UserContext";
import GestionPlatsAdmin from "./components/Administration/Plat/gestionPlats";
import FormulaireAjout from "./components/Administration/Plat/formulaireAjout";
import EditionPlat from "./components/Administration/Plat/EditionPlat";

function App() {
  const theme = React.useMemo(
    () =>
      createMuiTheme({
        breakpoints: {
          values: {
            xs: 0,
            sm: 600,
            md: 960,
            lg: 1366,
            xl: 1920,
          },
        },
        palette: {
          type: "dark",
        },
      }),
    []
  );

  let value;
  const [, setUserStatus] = useState<boolean>(true);
  let localUser = JSON.parse(localStorage.getItem("Token") || "{}");
  let localCompteurPanier = JSON.parse(
    localStorage.getItem("compteurPanier") || "0"
  );
  const [user, setUser] = useState(localUser || null);
  const [compteurPanier, setCompteurPanier] = useState(localCompteurPanier);
  value = useMemo(
    () => ({ user, setUser, compteurPanier, setCompteurPanier }),
    [user, setUser, compteurPanier, setCompteurPanier]
  );
  useEffect(() => {
    if (user.mail === undefined) {
      setUserStatus(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="App">
      {" "}
      <UserContext.Provider value={value}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          {user?.mail !== undefined ? (
            <div>
              <Router>
                <Navbar />
                <header className="App-header">
                  {user?.id_role === 1 ? (
                    <React.Fragment>
                      <Route
                        path="/"
                        render={() => {
                          return (
                            <Redirect from="/" to="/administration/plat" />
                          );
                        }}
                      />
                      <Route
                        path="/administration/plat"
                        exact
                        component={GestionPlatsAdmin}
                      ></Route>
                      <Route
                        path="/administration/AjoutPlat"
                        exact
                        component={FormulaireAjout}
                      ></Route>
                      <Route
                        path="/administration/EditPlat/:id"
                        exact
                        component={EditionPlat}
                      ></Route>
                    </React.Fragment>
                  ) : (
                    <React.Fragment>
                      <Route
                        exact
                        path="/"
                        render={() => {
                          return <Redirect from="/" to="/accueil" />;
                        }}
                      />
                      <Route
                        path="/accueil"
                        exact
                        component={AccueilConnected}
                      ></Route>
                    </React.Fragment>
                  )}
                </header>{" "}
              </Router>
            </div>
          ) : (
            <div>
              {" "}
              <Router>
                <Navbar />
                <header className="App-header">
                  <Route
                    exact
                    path="/"
                    render={() => {
                      return <Redirect from="/" to="/accueil" />;
                    }}
                  />
                  <Route
                    path="/accueil"
                    exact
                    component={AccueilDisconnected}
                  ></Route>
                  <Route
                    path="/SignUpSuccess"
                    exact
                    component={SignUpSuccess}
                  ></Route>
                  <Route path="/SignIn" exact component={SignUp}></Route>
                  <Route
                    path="/connection"
                    exact
                    component={Connection}
                  ></Route>
                </header>{" "}
              </Router>
            </div>
          )}
        </ThemeProvider>
      </UserContext.Provider>
    </div>
  );
}

export default App;
