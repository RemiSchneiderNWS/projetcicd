import image from "./Image";

export default interface plat {
  id: number;
  libelle: string;
  type: string;
  description: string;
  dateCreation: string;
  prix: string;
  image: image;
}
