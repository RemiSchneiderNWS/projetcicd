export default interface image {
    id:number;
    chemin: string;
    libelle: string;
    ordre: number;
}