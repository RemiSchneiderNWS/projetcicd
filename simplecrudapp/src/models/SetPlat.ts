export default interface SetPlat { 
    libelle: string;
    type: string;
    description: string;   
    prix: string;
}