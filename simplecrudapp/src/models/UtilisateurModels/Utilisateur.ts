export default interface Utilisateur {
    id:number;
    nom:string;
    prenom:string;
    mail:string;
    adresse:string;
    telephone:string;
    cp:string;
    jeton:string;
    password: string;
    id_role:number;
}