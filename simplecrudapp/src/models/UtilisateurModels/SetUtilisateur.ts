export default interface SetUtilisateur {    
 
    nom:string;
    prenom:string;
    mail:string;
    adresse:string;
    telephone:string;
    cp:string;
    mdp: string;
}