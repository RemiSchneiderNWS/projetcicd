import serverAdress from "./GlobalVariable";
import plat from './../models/Plat';
import SetPlat from "./../models/SetPlat";

export async function PostPlat(plat: SetPlat) {

    const requestOptions = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(plat),
      };

      const response = await fetch(serverAdress + "api/plats/newPlat", requestOptions);
      const data = await response.json();
      return data;
}

export async function getListPlat() {
    const response = await fetch(serverAdress + "api/plats");
    const data = await response.json();
    return data;
}

export async function DeletePlat(id: number) {
    const requestOptions = {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
        },
        
      };
   await fetch(serverAdress + "api/plats/"+ id,requestOptions);
}

export async function EditPlat(editPlat: plat) {
  const requestOptions = {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(editPlat),
  };

  await fetch(serverAdress + "api/plats/update/", requestOptions)
 
}

export async function getPlat(id:number) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    },
   
  };

  const response =await fetch(serverAdress + "api/plats/" + id, requestOptions)
  const data = await response.json();
  return data;

}