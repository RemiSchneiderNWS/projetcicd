import SetUtilisateur from "../models/UtilisateurModels/SetUtilisateur";
import Userlog from "../models/UtilisateurModels/UserLog";
import serverAdress from "./GlobalVariable";

export default async function PostUtilisateur(utilisateur: SetUtilisateur) {
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    //body: JSON.stringify({ nom: utilisateur.nom, prenom: utilisateur.prenom, mdp:utilisateur.mdp ,mail:utilisateur.mail,adresse:utilisateur.adresse}),
    body: JSON.stringify(utilisateur),
  };

  const response = await fetch(
    serverAdress + "api/Utilisateurs",
    requestOptions
  );
  const data = await response.json();
  return data;
}

export async function LogUser(userLog : Userlog){  
  
  const requestOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    
    body: JSON.stringify(userLog),
  };
  const response = await fetch(
    serverAdress + "api/Utilisateurs/login",
    requestOptions
  );
 
  const data = await response.json();
  return data
}
