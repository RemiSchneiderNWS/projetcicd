import React from "react";
import TextField from "@material-ui/core/TextField";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router";
import SetUtilisateur from "../../models/UtilisateurModels/SetUtilisateur";
import PostUtilisateur from "../../services/UtilisateurServices";
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(5),

        width: "25ch",
      },
    },
    input: {
      color: "white",
      PaddingBottom: "0",
    },
    error: {
      color: "red",
      fontSize: "15px",
      width: "50%",
      display: "flex",
      margin: "auto",
    },
    center: {
      display: "flex",
      margin: "auto",
      marginTop: "50px",
      background: "green",
    },
    border: {
      marginTop: "50px",
      border: "solid 1px",
      borderRadius: "30px",
      paddingBottom: "50px",
    },
  })
);

export default function SignUp() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const classes = useStyles();
  const history = useHistory();
  const onSubmit = (data: any) => {
    const setUtilisateur: SetUtilisateur = {
      mail: data.mail,
      mdp: data.Password,
      telephone: data.telephone,
      prenom: data.prenom,
      nom: data.nom,
      adresse: data.adresse,
      cp: data.cp,
    };
    /* Register(setUtilisateur).then((user) => {
      history.push("/SignUpSuccess");
    }); */

    PostUtilisateur(setUtilisateur).then((users) => {
      history.push("/SignUpSuccess");
    });
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.root} noValidate>
      <div className={classes.border}>
        <h3 style={{ marginLeft: "15%" }}>Création du compte</h3>
        <hr />
        <div>
          <TextField
            {...register("mail", { required: true })}
            name="mail"
            id="email"
            label="Adresse mail"
            className={classes.input}
          />
        </div>

        {errors.mail && (
          <span className={classes.error}>adresse mail requise</span>
        )}

        <div>
          <TextField
            {...register("prenom", { required: true })}
            name="prenom"
            id="prenom"
            label="Prenom"
            className={classes.input}
          />
        </div>

        {errors.prenom && (
          <span className={classes.error}>saisissez votre prenom</span>
        )}

        <div>
          <TextField
            {...register("nom", { required: true })}
            name="nom"
            id="nom"
            label="Nom"
            className={classes.input}
          />
        </div>

        {errors.nom && (
          <span className={classes.error}>saisissez votre nom</span>
        )}

        <div>
          <TextField
            {...register("telephone", { required: true })}
            name="telephone"
            id="telephone"
            label="Telephone"
            className={classes.input}
          />
        </div>

        {errors.telephone && (
          <span className={classes.error}>saisissez votre telephone</span>
        )}

        <div>
          <TextField
            {...register("adresse", { required: true })}
            name="adresse"
            id="adresse"
            label="Adresse"
            className={classes.input}
          />
        </div>

        {errors.adresse && (
          <span className={classes.error}>saisissez votre telephone</span>
        )}

        <div>
          <TextField
            {...register("cp", { required: true })}
            name="cp"
            id="cp"
            label="Code postal"
            className={classes.input}
          />
        </div>

        {errors.telephone && (
          <span className={classes.error}>saisissez votre telephone</span>
        )}

        <div>
          <TextField
            {...register("Password", { required: true })}
            id="standard-password-input"
            name="Password"
            label="Mot de passe "
            type="password"
            autoComplete="current-password"
          />
        </div>

        {errors.Password && (
          <span className={classes.error}>mot de passe requis</span>
        )}
        <Button className={classes.center} variant="contained" type="submit">
          <b>S'inscrire</b>{" "}
        </Button>
      </div>
    </form>
  );
}
