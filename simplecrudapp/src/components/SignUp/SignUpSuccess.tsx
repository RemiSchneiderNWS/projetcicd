import { Button, createStyles, makeStyles, Theme } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    center: {
      display: "flex",
      margin: "auto",
      marginTop: "50px",
    },
  })
);

export default function SignUpSuccess() {
  const classes = useStyles();
  const history = useHistory();
  return (
    <div>
      <div style={{ color: "green" }}>
        Votre compte a été créé avec succès {" "}
      </div>

      <Button
        className={classes.center}
        onClick={() => {
          history.push("/connection");
        }}
        variant="contained"
        type="submit"
      >
        Se connecter{" "}
      </Button>
    </div>
  );
}
