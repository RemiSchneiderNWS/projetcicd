import React, { useContext } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Badge from "@material-ui/core/Badge";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { useHistory } from "react-router-dom";
import { UserContext } from "../../services/context/UserContext";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    buttonConnect: {
      color: "#4caf50",
      margin: "10px",
    },
  })
);

function Navbar() {
  const { user, setUser, compteurPanier, setCompteurPanier } =
    useContext(UserContext);

  const history = useHistory();
  const handleLoginButton = () => {
    history.push("/connection");
  };
  const handleSignInButton = () => {
    history.push("/SignIn");
  };
  // console.log(user);
  const classes = useStyles();

  const handleDisconnect = () => {
    setUser(null);
    setCompteurPanier(0);
    localStorage.clear();
    history.push("/accueil");
  };

  if (user?.mail !== undefined) {
    return (
      <div className={classes.root}>
        <AppBar style={{ background: "#2E3B55" }}>
          <Toolbar>
            <Typography variant="h6" className={classes.title}>
              <b style={{ color: "blue" }}>Goo</b>
              <b>dE</b>
              <b style={{ color: "red" }}>ats</b>
            </Typography>
            <Typography variant="h6">{user.mail}</Typography>
            <IconButton aria-label="cart">
              <Badge
                badgeContent={compteurPanier > 0 ? compteurPanier : undefined}
                color="secondary"
              >
                <ShoppingCartIcon />
              </Badge>
            </IconButton>
            <Button color="inherit" onClick={handleDisconnect}>
              Déconnexion
            </Button>
          </Toolbar>
        </AppBar>
        <Toolbar />
      </div>
    );
  }

  return (
    <div className={classes.root}>
      <AppBar style={{ background: "#2E3B55" }}>
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <b style={{ color: "blue" }}>Goo</b>
            <b>dE</b>
            <b style={{ color: "red" }}>ats</b>
          </Typography>
          <Button
            className={classes.buttonConnect}
            variant="outlined"
            onClick={handleLoginButton}
          >
            Se connecter
          </Button>
          <Button
            color="secondary"
            variant="outlined"
            onClick={handleSignInButton}
          >
            S'inscrire
          </Button>
        </Toolbar>
      </AppBar>
      <Toolbar />
    </div>
  );
}

export default Navbar;
