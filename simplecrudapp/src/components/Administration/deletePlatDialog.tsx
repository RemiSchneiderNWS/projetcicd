import React from "react";
import InputLabel from "@material-ui/core/InputLabel";
import ConfirmDialog from "../shared/ConfirmDialog";
import { DeletePlat } from "../../services/platServices";

function DeletePlatDialog({ onClose, plat }: any) {
  const handlePlatDelete = () => {
    DeletePlat(plat.id).then(() => {
      onClose(true);
    });
  };

  return (
    <ConfirmDialog
      open={true}
      dialogTitle={`Suppression ${plat.libelle}`}
      validButtonText={"Supprimer"}
      onValid={handlePlatDelete}
      onClose={() => onClose(false)}
    >
      <React.Fragment>
        <InputLabel>{plat.libelle}</InputLabel>
        <p>Voulez vraiment supprimer le plat ?</p>
      </React.Fragment>
    </ConfirmDialog>
  );
}

export default DeletePlatDialog;
