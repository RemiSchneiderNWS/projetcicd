import React, { useEffect, useState } from "react";
import {
  Button,
  createStyles,
  makeStyles,
  MenuItem,
  TextField,
  Theme,
} from "@material-ui/core";

import { useForm } from "react-hook-form";
import { useHistory, useParams } from "react-router-dom";
import { EditPlat, getPlat } from "../../../services/platServices";
import plat from "../../../models/Plat";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(5),

        width: "25ch",
      },
    },
    input: {
      color: "white",
      PaddingBottom: "0",
    },
    error: {
      color: "red",
      fontSize: "15px",
      width: "50%",
      display: "flex",
      margin: "auto",
    },
    center: {
      display: "flex",
      margin: "auto",
      marginTop: "50px",
    },
    border: {
      marginTop: "50px",
      border: "solid 1px",
      borderRadius: "30px",
      paddingBottom: "50px",
    },
  })
);

function EditionPlat() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  let { id }: any = useParams();
  const [plat, setPlat] = useState<plat>();
  const history = useHistory();

  const classes = useStyles();
  const [currency, setCurrency] = React.useState("");
  const currencies = [
    {
      value: "entree",
      label: "entrée",
    },
    {
      value: "plat",
      label: "plat",
    },
    {
      value: "dessert",
      label: "dessert",
    },
  ];

  const onSubmit = (data: any) => {
    const editPlat: plat = {
      id: Number(id),
      dateCreation: plat?.dateCreation!,
      libelle: data.libelle,
      type: data.type,
      description: data.description,
      prix: data.prix,
      image: { id: 0, chemin: "", libelle: "", ordre: 0 },
    };

    console.log(editPlat);
    EditPlat(editPlat).then(() => {
      history.push("/administration/plat")
    });
  };

  const handleChange = (event: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setCurrency(event.target.value);
  };

  useEffect(() => {
    getPlat(id).then((plat: plat) => {
      setPlat(plat);
      setCurrency(plat.type);
      console.log("le plat récupéré", plat);
    });
  }, [id]);

  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.root} noValidate>
      <div className={classes.border}>
        <h3 style={{ marginLeft: "15%" }}>Modification du plat</h3>
        <div>
          <TextField
            {...register("libelle", { required: true })}
            name="libelle"
            key={plat?.libelle}
            id="libelle"
            defaultValue={plat?.libelle}
            label="Nom du plat"
            className={classes.input}
          />
        </div>

        {errors.libelle && (
          <span className={classes.error}> saisissez le nom du plat</span>
        )}

        <div>
          <TextField
            {...register("type", { required: true })}
            name="type"
            id="type"
            select
            value={currency}
            label="Type de plat"
            onChange={handleChange}
            className={classes.input}
          >
            {currencies.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>

        {errors.type && (
          <span className={classes.error}> saisissez le type de plat</span>
        )}

        <div>
          <TextField
            {...register("description", { required: true })}
            name="description"
            key={plat?.description}
            defaultValue={plat?.description}
            id="description"
            label="Description du plat"
            className={classes.input}
          />
        </div>

        {errors.description && (
          <span className={classes.error}>
            {" "}
            saisissez la description du plat
          </span>
        )}

        <div>
          <TextField
            {...register("prix", { required: true })}
            name="prix"
            id="prix"
            key={plat?.prix}
            defaultValue={plat?.prix}
            label="Prix du plat"
            type="number"
            className={classes.input}
          />
        </div>

        {errors.prix && (
          <span className={classes.error}> saisissez le prix du plat</span>
        )}

        <Button className={classes.center} variant="contained" type="submit">
          Modifier{" "}
        </Button>
      </div>
    </form>
  );
}
export default EditionPlat;
