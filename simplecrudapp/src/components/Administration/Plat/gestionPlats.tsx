import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { getListPlat } from "../../../services/platServices";
import EditTable from "../../shared/EditTable";
import { Button, Fab } from "@material-ui/core";
import plat from "./../../../models/Plat";
import DeletePlatDialog from "./../deletePlatDialog";
import { useHistory } from "react-router";

import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    buttonSuppr: {
      background: "red",
      marginLeft: "5px",
    },
    buttonEdit: {
      background: "orange",
    },
    buttonAdd: {
      background: "green",
      marginTop: "25px",
    },
    centerElement: {
      textAlign: "center",
    },
    titleWelcome: {
      marginBottom: "10%",
      marginTop: "-20%",
      textAlign: "center",
    },
    miniCharactere: {
      fontSize: 15,
    },
  })
);

const GestionPlats = () => {
  const [plats, setPlat] = React.useState([]);
  const [openDialog, setOpenDialog] = React.useState(false);
  const history = useHistory();
  React.useEffect(() => {
    getListPlat().then((data) => {
      if (data && data.length > 0) setPlat(data);
    });
  }, []);
  const classes = useStyles();

  const [selectedPlat, setSelectedPlat] = React.useState([]);

  const handleDeletePlatDialogOpen = (id: number) => {
    if (!openDialog) {
      setSelectedPlat(plats.filter((x: any) => x.id === id)[0]);
      setOpenDialog(true);
    }
  };

  const handleDeletePlatDialogClose = (needUpdate: boolean) => {
    if (needUpdate) {
      getListPlat().then((data) => {
        setPlat(data);
      });
    }
    setOpenDialog(false);
  };
  return (
    <div>
      <div className={classes.titleWelcome}>
        <h1>
          Bienvenue sur <b style={{ color: "blue" }}>Goo</b>
          <b>dE</b>
          <b style={{ color: "red" }}>ats</b> !
        </h1>
        <p className={classes.miniCharactere}>
          <i>
            Vous êtes actuellement connecté en tant qu'
            <b style={{ color: "green" }}>
              <u>administrateur.</u>
            </b>
          </i>
        </p>
      </div>
      <Button
        className={classes.buttonAdd}
        onClick={() => history.push("/administration/ajoutPlat")}
      >
        <AddIcon /> Ajouter un nouveau plat
      </Button>

      <EditTable
        columns={[
          { data: "libelle", label: "Nom" },
          { data: "type", label: "Type (entrée, plat, dessert)" },
          { data: "description", label: "Description" },
          { data: "dateCreation", label: "Date de création" },
          { data: "prix", label: "Prix" },
          {
            label: "Actions",
            render: (row: plat) => (
              <React.Fragment>
                <Fab
                  onClick={() =>
                    history.push("/administration/EditPlat/" + row.id)
                  }
                  className={classes.buttonEdit}
                  aria-label="edit"
                >
                  <EditIcon />
                </Fab>
                <Fab
                  onClick={() => handleDeletePlatDialogOpen(row.id)}
                  className={classes.buttonSuppr}
                  aria-label="delete"
                >
                  <DeleteIcon />
                </Fab>
              </React.Fragment>
            ),
          },
        ]}
        dataIdAccessor={"id"}
        data={plats}
      />
      {openDialog && (
        <DeletePlatDialog
          plat={selectedPlat}
          onClose={handleDeletePlatDialogClose}
        />
      )}
    </div>
  );
};

export default GestionPlats;
