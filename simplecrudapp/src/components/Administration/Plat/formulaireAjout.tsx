import React from "react";
import {
  Button,
  createStyles,
  makeStyles,
  MenuItem,
  TextField,
  Theme,
} from "@material-ui/core";
import SetPlat from "../../../models/SetPlat";
import { PostPlat } from "../../../services/platServices";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(5),

        width: "25ch",
      },
    },
    input: {
      color: "white",
      PaddingBottom: "0",
    },
    error: {
      color: "red",
      fontSize: "15px",
      width: "50%",
      display: "flex",
      margin: "auto",
    },
    center: {
      display: "flex",
      margin: "auto",
      marginTop: "50px",
    },
    border: {
      marginTop: "50px",
      border: "solid 1px",
      borderRadius: "30px",
      paddingBottom: "50px",
    },
  })
);
function FormulaireAjout() {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const history = useHistory();
  const classes = useStyles();

  const currencies = [
    {
      value: "entree",
      label: "entrée",
    },
    {
      value: "plat",
      label: "plat",
    },
    {
      value: "dessert",
      label: "dessert",
    },
  ];
  const [currency, setCurrency] = React.useState("");
  const onSubmit = (data: any) => {
    const setPlat: SetPlat = {
      libelle: data.libelle,
      type: data.type,
      description: data.description,
      prix: data.prix,
    };
  
    console.log(setPlat);
    PostPlat(setPlat).then(() => {
      history.push("/administration/plat")
    });
  };

  const handleChange = (event: {
    target: { value: React.SetStateAction<string> };
  }) => {
    setCurrency(event.target.value);
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)} className={classes.root} noValidate>
      <div className={classes.border}>
        <h3 style={{ marginLeft: "15%" }}>Ajout d'un plat</h3>
        <div>
          <TextField
            {...register("libelle", { required: true })}
            name="libelle"
            id="libelle"
            label="Nom du plat"
            className={classes.input}
          />
        </div>

        {errors.libelle && (
          <span className={classes.error}> saisissez le nom du plat</span>
        )}

        <div>
          <TextField
            {...register("type", { required: true })}
            name="type"
            id="type"
            select
            value={currency}
            label="Type de plat"
            onChange={handleChange}
            className={classes.input}
          >
            {currencies.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
        </div>

        {errors.type && (
          <span className={classes.error}> saisissez le type de plat</span>
        )}

        <div>
          <TextField
            {...register("description", { required: true })}
            name="description"
            id="description"
            label="Description du plat"
            className={classes.input}
          />
        </div>

        {errors.description && (
          <span className={classes.error}>
            {" "}
            saisissez la description du plat
          </span>
        )}

        <div>
          <TextField
            {...register("prix", { required: true })}
            name="prix"
            id="prix"
            label="Prix du plat"
            type="number"
            className={classes.input}
          />
        </div>

        {errors.prix && (
          <span className={classes.error}> saisissez le prix du plat</span>
        )}

        <Button className={classes.center} variant="contained" type="submit">
          Ajout du plat{" "}
        </Button>
      </div>
    </form>
  );
}

export default FormulaireAjout;
