import React, { useContext, useState } from "react";

import TextField from "@material-ui/core/TextField";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import { Button } from "@material-ui/core";
import { useForm } from "react-hook-form";
import { useHistory } from "react-router";

import { UserContext } from "../../services/context/UserContext";
import Userlog from "../../models/UtilisateurModels/UserLog";
import { LogUser } from "../../services/UtilisateurServices";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& .MuiTextField-root": {
        margin: theme.spacing(5),

        width: "25ch",
      },
    },
    input: {
      color: "white",
      PaddingBottom: "0",
    },
    error: {
      color: "red",
      fontSize: "15px",
      width: "50%",
      display: "flex",
      margin: "auto",
    },
    center: {
      display: "flex",
      margin: "auto",
      marginTop: "50px",
    },
    border: {
      border: "solid 1px",
      borderRadius: "30px",
      paddingBottom: "50px",
    },
    errorFrame: {
      color: "red",
      display: "flex",
      margin: "2%",
    },
  })
);

export default function Connection() {
  const [Status, setStatus] = useState(true);
  const { setUser } = useContext(UserContext);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const classes = useStyles();
  const history = useHistory();
  const onSubmit = (data: any) => {
    const userLog: Userlog = { mail: data.mail, mdp: data.Password };
    LogUser(userLog).then((userIdentificated) => {
      if (userIdentificated.status === 404) {
        setStatus(false);
        return alert("Votre adresse mail ou votre mot de passe est incorrect");
      } else {
        setStatus(true);
        localStorage.setItem("Token", JSON.stringify(userIdentificated));
        setUser(userIdentificated);
        history.push("/accueil");
      }
    });
    history.push("/accueil");
  };
  return (
    <div>
      {Status === false ? (
        <div className={classes.errorFrame}>
          Adresse mail ou mot de passe incorrect !
        </div>
      ) : null}
      <form
        onSubmit={handleSubmit(onSubmit)}
        className={classes.root}
        noValidate
      >
        <div className={classes.border}>
          <h2 style={{ marginLeft: "5%" }}>Se connecter</h2>
          <div>
            <TextField
              {...register("mail", { required: true })}
              name="mail"
              id="email"
              label="Adresse mail"
              className={classes.input}
            />
          </div>

          {errors.mail && (
            <span className={classes.error}>Ce champs est requis</span>
          )}
          <div>
            <TextField
              {...register("Password", { required: true })}
              id="standard-password-input"
              name="Password"
              label="Mot de passe"
              type="password"
              autoComplete="current-password"
            />
          </div>

          {errors.Password && (
            <span className={classes.error}>Ce champs est requis</span>
          )}
          <Button className={classes.center} variant="contained" type="submit">
            S'identifier{" "}
          </Button>
        </div>
      </form>
    </div>
  );
}
