import React from 'react';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const styles = () => ({
  paper: {
    padding: 0,
    margin: 0,
    boxShadow: '0px 1px 5px 0px rgba(0,0,0,0.2),0px 2px 2px 0px rgba(0,0,0,0.14),0px 3px 1px -2px rgba(0,0,0,0.12)'
  }
});

/**
 * Composant EditTable
 *
 * Affiche un ensemble d'éléments dans une table
 * Propose des actions de création, édition et suppression dans une colonne spécifique
 * @param columns Liste des colonnes { data: string, label: string, render: (data) => {} }
 * @param data: le contenu de la table
 */
const EditTable = ({ classes, columns, data, dataIdAccessor }) => {
  return (
    <Paper className={classes.paper}>
      <Table>
        <TableHead>
          <TableRow>
            {columns.map((column, i) => (
              <TableCell key={i}>{column.label ?? column.data ?? ''}</TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((row, rowIndex) => (
            <TableRow hover key={dataIdAccessor ? row[dataIdAccessor] : rowIndex}>
              {columns.map((column, i) => (
                <TableCell key={i}>{column.render ? column.render(row) : row[column.data]}</TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};

EditTable.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  dataIdAccessor: PropTypes.string.isRequired
};

export default withStyles(styles)(EditTable);
