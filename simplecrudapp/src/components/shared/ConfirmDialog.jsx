import React from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const styles = theme => ({
  inputLabel: {
    display: 'block',
    width: '100%'
  }
});

const ConfirmDialog = ({ classes, open, dialogTitle, validButtonText, onValid, onClose, children }) => {
  return (
    <Dialog fullWidth open={open} disableBackdropClick>
      <DialogTitle>{dialogTitle}</DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        <Button onClick={onClose} color="primary">
          Annuler
        </Button>
        <Button onClick={onValid} color="secondary" variant="contained">
          {validButtonText}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default withStyles(styles)(ConfirmDialog);
