import React from "react";
import { getListPlat } from "../../services/platServices";
import plat from "../../models/Plat";
import PlatCard from "./PlatCard";
import { UserContext } from "../../services/context/UserContext";

import Container from "@material-ui/core/Container";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    homeTitle: {
      marginTop: theme.spacing(5),
      userSelect: "none",
    },
    gridContainer: {
      flexGrow: 1,
      marginTop: theme.spacing(2),
    },
    divContainer: {
      marginTop: theme.spacing(5),
    },
    divider: {
      height: "5px",
    },
  })
);

export default function AccueilConnected() {
  const { compteurPanier, setCompteurPanier } = React.useContext(UserContext);
  const [plats, setPlats] = React.useState([]);

  const classes = useStyles();

  React.useEffect(() => {
    localStorage.setItem("compteurPanier", JSON.stringify(compteurPanier));
  }, [compteurPanier]);

  // Requête de la liste des plats
  React.useEffect(() => {
    getListPlat().then((data) => {
      setPlats(data);
    });
  }, []);

  // Au clic sur ajout d'un plat
  const handleClickOnAdd = () => {
    setCompteurPanier(compteurPanier + 1);
  };

  return (
    <Container maxWidth="xl">
      <div className={classes.homeTitle}>
        <Typography variant="h2" gutterBottom align="center">
          Bienvenue sur <b style={{ color: "blue" }}>Goo</b>
          <b>dE</b>
          <b style={{ color: "red" }}>ats</b>
        </Typography>
        <Typography variant="h5" gutterBottom align="center">
          Une petite faim ? Choisissez parmis plus de{" "}
          {plats.length > 0 ? plats.length : "..."} entrées, plats et desserts !
        </Typography>
      </div>
      <div className={classes.divContainer}>
        <Typography variant="h6" gutterBottom>
          Entrée
        </Typography>
        <Divider className={classes.divider} light />
        <Grid className={classes.gridContainer} container spacing={4}>
          {plats
            .filter((x: plat) => x.type === "entree")
            .map((plat: plat) => (
              <PlatCard
                key={plat.id}
                onClickAdd={handleClickOnAdd}
                plat={plat}
              />
            ))}
        </Grid>
      </div>
      <div className={classes.divContainer}>
        <Typography variant="h6" gutterBottom>
          Plat
        </Typography>
        <Divider className={classes.divider} light />
        <Grid className={classes.gridContainer} container spacing={4}>
          {plats
            .filter((x: plat) => x.type === "plat")
            .map((plat: plat) => (
              <PlatCard
                key={plat.id}
                onClickAdd={handleClickOnAdd}
                plat={plat}
              />
            ))}
        </Grid>
      </div>
      <div className={classes.divContainer}>
        <Typography variant="h6" gutterBottom>
          Dessert
        </Typography>
        <Divider className={classes.divider} light />
        <Grid className={classes.gridContainer} container spacing={4}>
          {plats
            .filter((x: plat) => x.type === "dessert")
            .map((plat: plat) => (
              <PlatCard
                key={plat.id}
                onClickAdd={handleClickOnAdd}
                plat={plat}
              />
            ))}
        </Grid>
      </div>
    </Container>
  );
}
