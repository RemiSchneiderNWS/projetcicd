import { Button, Paper } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";
import { getListPlat } from "../../services/platServices";

import Typography from "@material-ui/core/Typography";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    homeTitle: {
      marginTop: theme.spacing(5),
      userSelect: "none",
    },
    buttonConnect: {
      color: "#4caf50",
      margin: "10px",
    },
    miniCharactere: {
      fontSize: 15,
    },
    centerElement: {
      textAlign: "center",
    },
    boxWelcome: {
      padding: "20px",
      background: "none",
      border: "solid 1px grey",
      borderRadius: "70px",
      paddingBottom: "80px",
      "&:hover": {
        border: "solid 1px",
        borderRadius: "80px",
        paddingBottom: "90px",
      },
    },
    hrStyle: {
      marginTop: "10%",
      width: "65%",
    },
  })
);

export default function AccueilDisconnected() {
  const [plats, setPlats] = React.useState([]);
  const history = useHistory();
  const classes = useStyles();

  const handleLoginButton = () => {
    history.push("/connection");
  };
  const handleSignInButton = () => {
    history.push("/SignIn");
  };

  // Fonction au démontage du composant
  React.useEffect(() => {
    getListPlat().then((data) => {
      setPlats(data);
    });
  }, []);
  return (
    <Paper className={classes.boxWelcome} elevation={10}>
      <div className={classes.homeTitle}>
        <Typography variant="h2" gutterBottom align="center">
          Bienvenue sur <b style={{ color: "blue" }}>Goo</b>
          <b>dE</b>
          <b style={{ color: "red" }}>ats</b>
        </Typography>
        <Typography variant="h5" gutterBottom align="center">
          Une petite faim ? Choisissez parmis plus de{" "}
          {plats.length > 0 ? plats.length : "..."} entrées, plats et desserts !
        </Typography>
        <hr className={classes.hrStyle} />
        <Typography variant="h5" gutterBottom align="center">
          <p className={classes.miniCharactere}>
            <i>Veuillez-vous connectez afin d'accéder à la commande de plat.</i>
          </p>
        </Typography>
        <div className={classes.centerElement}>
          <Button
            className={classes.buttonConnect}
            variant="outlined"
            onClick={handleLoginButton}
          >
            Se connecter
          </Button>
          <Button
            color="secondary"
            variant="outlined"
            onClick={handleSignInButton}
          >
            S'inscrire
          </Button>
        </div>
      </div>
    </Paper>
  );
}
