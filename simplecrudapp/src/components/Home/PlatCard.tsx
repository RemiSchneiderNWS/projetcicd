import React from "react";
import clsx from "clsx";
import plat from "../../models/Plat";

import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import AddIcon from "@material-ui/icons/Add";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: "56.25%", // 16:9
    },
    expand: {
      transform: "rotate(0deg)",
      transition: theme.transitions.create("transform", {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: "rotate(180deg)",
    },
    iconButtonAdd: {
      backgroundColor: "#696969 !important",
      marginLeft: "auto",
    },
    divCardTitle: {
      display: "flex",
    },
    cardPrice: {
      marginLeft: "auto",
      color: "#c0c0c0",
    },
    cardSubtitle: {
      color: "#c0c0c0",
    },
  })
);

export default function PlatCard({
  plat,
  onClickAdd,
}: {
  plat: plat;
  onClickAdd: any;
}) {
  const [expanded, setExpanded] = React.useState(false);
  const classes = useStyles();

  // Au clic sur description
  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Grid item md={3}>
      <Card className={classes.root} elevation={5}>
        <CardHeader
          title={
            <div className={classes.divCardTitle}>
              <Typography component="p" variant="subtitle1">
                {plat.libelle}
              </Typography>
              <Typography
                className={classes.cardPrice}
                component="p"
                variant="subtitle1"
              >
                {`${plat.prix}€`}
              </Typography>
            </div>
          }
        />
        <CardMedia
          className={classes.media}
          image={`/assets/plats/${plat.image.chemin}`}
          title={plat.image.libelle}
        />
        <CardActions disableSpacing>
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded,
            })}
            onClick={handleExpandClick}
            aria-expanded={expanded}
            aria-label="Voir plus"
          >
            <ExpandMoreIcon />
          </IconButton>
          <Typography className={classes.cardSubtitle} variant="body2">
            Description
          </Typography>
          <IconButton
            className={classes.iconButtonAdd}
            aria-label="Ajouter au panier"
            onClick={onClickAdd}
          >
            <AddIcon />
          </IconButton>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <Typography>{plat.description}</Typography>
          </CardContent>
        </Collapse>
      </Card>
    </Grid>
  );
}
